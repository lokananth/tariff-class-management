<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 *
 * @author Shanmuganathan Velusamy
 */
//include_once('base_controller.php');

class user extends CI_Controller {

    public function __construct() {
        parent::__construct();
 	date_default_timezone_set('UTC');
        $this->load->helper(array('url', 'form', 'api', 'portal'));
        if (!isset($_SESSION)) {
            session_start();
        }
        $this->load->library('session');
    }

    public function index() {
        //  
        $this->login();
    }

    /**
     * Login
     */
    public function login() {
        $this->load->view('index');
    }
    
    /**
     * Login Form Submission
     */
    public function login_form_submission() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
		$redirect_url = trim($this->input->post('redirect_url'));//Email redirect to the corresponding page
	    $email = $this->input->post('email');
	    $password = trim($this->input->post('password'));
	    
	    $data = array();
	    
	    if(($email != '') && ($password !='')){
		$data['title'] = "Login";
        $param = array(
            "Email" => $email,
			"Password" => $password
        );
		 $login_output = apiPost(config_item('number_pool_login'), $param);	
			//print_r($login_output->Id);
			//exit;
		    if($login_output->errcode != 0 ){
			  $data['login_error'] = "Invalid Credentials!";
			  $this->load->view('index',$data);
			  return;
		    }
			else{
				$this->session->set_userdata('id', $login_output->Id);
				$this->session->set_userdata('userId', $email);
                $this->session->set_userdata('password', $password);
				
				//echo "<pre>";
				//print_r($data['number_list']);
				//exit;
				redirect("tariff/");
				//$this->load->view("index_main",$data);	
				return;
				}		
        }
	}
    }
    
    /**
     * LogOut
     */
   public function logout(){
		session_destroy();
		unset($_SESSION);		
		$this->session->sess_destroy();
		redirect(site_url('/'));
	}

}
