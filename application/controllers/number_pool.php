<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once('base_controller.php');
class Number_pool extends base_controller {
	
	function __construct()
	{
	    parent::__construct();            
	}
	
	public function index()
	{
		 
	}	
	
	public function pool_log($redirect_to = '') {		
				$data['number_list'] = apiPost(config_item('number_pool_url'). "GetOperatorDetails", '');
				$data['activeTab'] = "number_pool";
				$this->load->view("pool_log",$data);					
	}
	
	public function cancel_operator($redirect_to = '') {
	  //Operators List
      $data['operator_output'] = apiPost(config_item('number_pool_url'). "GetOperators", '');
	  //Country List
	  $data['country_output'] = apiPost(config_item('number_pool_url'). "GetOperatorAndCountry", '');
	   if ($_SERVER['REQUEST_METHOD'] === 'POST'):
	   
	  
	  //City List 
	   if($this->input->post('slug')=='country')
	   {
	   $param = array(
				"id" => $this->input->post('country_val')
            );
		echo $city_list = apinewPost(config_item('number_pool_url'). "GetOperatorAndCity", $param);
		exit;		
	   } 
		//State List
	   if($this->input->post('slug')=='state')
	   {
	   $param = array(
				"id" => $this->input->post('country_val'),
				"state" => $this->input->post('state_val'),
					
            );
		echo $city_from_state = apinewPost(config_item('number_pool_url'). "GetOperatorAndCity", $param);
		exit;		
	   }	   
	   
	   //Prefix List
	   if($this->input->post('slug')=='city')
	   {
	   $param = array(
				"city_id" => $this->input->post('city_val')		
            );
		echo $prefix_list = apinewPost(config_item('number_pool_url'). "GetCityPrefixCode", $param);
		exit;		
	   }
  
	   endif;
	   
		
			
	  $data['activeTab'] = "number_pool";
	  $this->load->view("cancel_operator",$data);
	}
	
	

}


