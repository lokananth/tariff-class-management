<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once('base_controller.php');
class Reallocate extends base_controller {
	
	function __construct()
	{
	    parent::__construct();            
	}
	
	public function index()
	{
		 
	}	
	public function view($redirect_to = '') {		
				$data['number_list'] = apiPost(config_item('number_pool_url'). "GetOperatorDetails", '');
				$data['activeTab'] = "reallocate";
				$this->load->view("reallocate_view",$data);					
	}
	public function reallocate_log($redirect_to = '') {		
				$data['number_list'] = apiPost(config_item('number_pool_url'). "GetOperatorDetails", '');
				$data['activeTab'] = "number_pool";
				$this->load->view("pool_log",$data);					
	}
	public function reallocate_number(){
		$service = $this->input->post('service');
		$id = $this->input->post('id');
		$service_id = $this->input->post('service_id');		
		$param = array(
		"id" => $id,
		"service_id" => $service_id
		);		
		//print_r($service);
		//exit;
		//echo apiPost(config_item('number_pool_url').$service);
		echo $get_result = apinewPost(config_item('number_pool_url').$service, $param);		
	}
	
	
	

}


