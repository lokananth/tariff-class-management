<?php
		
	    
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This class object is the base class that every library and helper files are loadad in.
 *
 * @author Thirunavukkarasu
 */
class base_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('UTC');
        if (!isset($_SESSION)) {
            session_start();
        }
 		
        $this->load->helper(array('url', 'form', 'api', 'portal','email','mailapps_helper'));
		$this->check_user_session();
	    $this->load->library('session','email');	
	    $this->load->model('user_model','model');
        $this->load->helper('mailapps_helper');		
	    $this->load->model('session_check_model','sessionCheckModel');
		$this->load->library('excel');
    }
	
    public function check_user_session() { 
		$user = $this->session->userdata('userId');
        if (!isset($user) || $user == ""):
            redirect('user/login');		
        endif;
	}

}
