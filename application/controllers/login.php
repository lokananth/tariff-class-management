<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once('base_controller.php');
class Login extends base_controller {
	
	function __construct()
	{
	    parent::__construct();            
	}
	
	public function index()
	{
		 $user = $this->session->userdata('userId');
		 if(!isset($user)){
					$this->load->view('index');
			   }else{
					$data['nil'] = '';
					$data['number_list'] = apiPost(config_item('number_pool_url'). "GetOperatorDetails", '');
					$data['activeTab'] = "number_pool";
					$this->load->view('number_pool',$data);               
		}
	}	
	
	public function number_pool($redirect_to = '') {		
				$data['number_list'] = apiPost(config_item('number_pool_url'). "GetOperatorDetails", '');
				$data['activeTab'] = "number_pool";
				$this->load->view("number_pool",$data);					
	}
	
	public function utilized($redirect_to = '') {
		$data['utilized_list'] = apiPost(config_item('number_pool_url'). "GetServicesSummaryRpt", '');		
		if($data['utilized_list'][0]->errcode !=0)
		{
			$data = "";
		}
		$data['activeTab'] = "utilized";
		$this->load->view("utilized",$data);
	}
	public function add_number($redirect_to = '') {
	  //Operators List
      $data['operator_output'] = apiPost(config_item('number_pool_url'). "GetOperators", '');
	  //Country List
	  $data['country_output'] = apiPost(config_item('number_pool_url'). "GetOperatorAndCountry", '');
	   if ($_SERVER['REQUEST_METHOD'] === 'POST'):
	   
	  
	  //City List 
	   if($this->input->post('slug')=='country')
	   {
	   $param = array(
				"id" => $this->input->post('country_val')
            );
		echo $city_list = apinewPost(config_item('number_pool_url'). "GetOperatorAndCity", $param);
		exit;		
	   } 
		//State List
	   if($this->input->post('slug')=='state')
	   {
	   $param = array(
				"id" => $this->input->post('country_val'),
				"state" => $this->input->post('state_val'),
					
            );
		echo $city_from_state = apinewPost(config_item('number_pool_url'). "GetOperatorAndCity", $param);
		exit;		
	   }	   
	   
	   //Prefix List
	   if($this->input->post('slug')=='city')
	   {
	   $param = array(
				"city_id" => $this->input->post('city_val')		
            );
		echo $prefix_list = apinewPost(config_item('number_pool_url'). "GetCityPrefixCode", $param);
		exit;		
	   }
  
	   endif;
	   
		
			
	  $data['activeTab'] = "number_pool";
	  $this->load->view("add_number",$data);
	}
	
	public function save_number($redirect_to = '') {
	  if ($_SERVER['REQUEST_METHOD'] === 'POST'):
	  $config['upload_path'] = './uploaded_sheets/';
		$config['allowed_types'] = 'xls|xlsx|csv';
		$config['max_size']	= '0';
			//echo '<pre>';
			//print_r($_FILES); 			
			
			//exit;
			$this->load->library('upload', $config);
			$field='file_up';
			//echo '<pre>';
			//print_r($_FILES[$field]);
			$files = array();
   			foreach($_FILES[$field] as $key => $all )
       	 		foreach( $all as $i => $val )
           		 $files[$i][$key] = $val;
				 
			$this->load->library('ftp');
			$config['port']     = 21;
			$config['hostname'] = '192.168.2.102';
			$config['username'] = 'administrator';
			$config['password'] = 'Vicarage_2008';
			$config['debug']	= TRUE;	

			
			$files_uploaded = array();
			
			for ($i=0; $i < count($files); $i++) { 
				if($files[$i]['size']!=0)  {
					
				$_FILES[$field] = $files[$i];
				//echo '<pre>';
			//print_r($_FILES[$field]);
			//exit;
				if ($this->upload->do_upload($field)) {
					
					//echo 'upload: success<br>';
					//if($i == 1)
					//$success[$i] = 0;
					$file = '';
					$file_name = '';					
					$remote_file = '';
					$data = '';
					$files_uploaded[$i] = $this->upload->data($files);
					$SaveUploadedData[$i] = array('upload_data' => $this->upload->data());
					
					$data = array('upload_data' => $this->upload->data());
					$file = $data['upload_data']['full_path']; 
					$file_name = 'NP-'.time().'.xlsx';//$data['upload_data']['file_name']; exit;
					$postFileName[] = $file_name;
					$postLinuxFile[] = $data['upload_data']['file_name'];
					$remote_file = '/SMSROUAWEB/uploads/PFNPUploads/'.$file_name;
					//$remote_file = '/SMSROUAWEB/uploads/LCRFiles/'.$file_name;
					$connect =$this->ftp->connect($config);
					$this->ftp->upload($file, $remote_file, 'binary');
					$this->ftp->close();
				}
				else {
					$files_uploaded[$i] = null;
					$error = array('error' => $this->upload->display_errors());
					$this->session->set_flashdata('upload_sheets_status_fail',"Please upload '.xlsx' file with correct format!");
					redirect('login/add_number');
					
				}
				sleep(1);
				}
			}
			for ($i=0; $i < count($files); $i++) {				
			if(isset($postFileName[$i]))
			{
			$data = array(
                "Operator_id" => $this->input->post('operator'),
				"country_id" => $this->input->post('country'),
				"city_id" => $this->input->post('city'),
				"prefix_id" => $this->input->post('prefix'),
				"source_file" => $postFileName[$i],
				"createdby" => 'Admin',
            );	
			}
			else
			{
            $data = array(
                "Operator_id" => $this->input->post('operator'),
				"country_id" => $this->input->post('country'),
				"city_id" => $this->input->post('city'),
				"prefix_id" => $this->input->post('prefix'),
				"start_range" => $this->input->post('min'),
				"end_range" => $this->input->post('max'),
				"createdby" => 'Admin',
            );
			}
            $response = apinewPost(config_item('number_pool_url')."AddOperator", $data); 
			echo $response;
			}			
			exit;		
        endif;
	}
	
	public function allocate_number($redirect_to = '') {
	    //Operators List
     // $data['operator_output'] = apiPost(config_item('number_pool_url'). "GetDistinctOperators", '');
	  //Services List
	  $data['services_output'] = apiPost(config_item('number_pool_url'). "GetServiceList", '');
	  //Country List
	 // $data['country_output'] = apiPost(config_item('number_pool_url'). "GetOperatorAndCountry", '');
	  //Count List
	  //$data['count_output'] = apiPost(config_item('number_pool_url'). "GetCounters", '');
	   if ($_SERVER['REQUEST_METHOD'] === 'POST'):
	   //Operator List
	   if($this->input->post('slug')=='services')
	   {
	   $param = array(
				"service_id" => $this->input->post('services_val')
            );
		echo $country_output = apinewPost(config_item('number_pool_url'). "GetDistinctOperators", $param);
		exit;		
	   }
	   //Country List 
	   if($this->input->post('slug')=='operator')
	   {
	   $param = array(
				"Operatorid" => $this->input->post('operator_val'),
				"service_id" => $this->input->post('services_val')
            );
		echo $country_output = apinewPost(config_item('number_pool_url'). "GetCountryDetailsByOperatorId", $param);
		exit;		
	   }
		//City List 
	   if($this->input->post('slug')=='country')
	   {
	   $param = array(
				"Operatorid" => $this->input->post('operator_val'),
				"country_id" => $this->input->post('country_val'),
				"service_id" => $this->input->post('services_val')
            );
		echo $city_list = apinewPost(config_item('number_pool_url'). "GetCityDetailsByOperatorIdCountryId", $param);
		exit;		
	   }
	   
	    //Prefix List
	   if($this->input->post('slug')=='city')
	   {
	   $param = array(
				"city_id" => $this->input->post('city_val'),
				"Operatorid" => $this->input->post('operator_val'),
				"country_id" => $this->input->post('country_val'),
				"service_id" => $this->input->post('services_val')
            );
		echo $prefix_list = apinewPost(config_item('number_pool_url'). "GetPrefixDetailsByOperatorId", $param);
		exit;		
	   }
	   
	    //Count List
	   if($this->input->post('slug')=='count')
	   {
	  $param = array(
				"operator_id" => $this->input->post('operator'),
				"country_id" => $this->input->post('country'),
				"city_id" => $this->input->post('city'),
				"prefixcode" => $this->input->post('prefix'),				
            );
			//print_r($param);
		echo $count_list = apinewPost(config_item('number_pool_url'). "GetCounters", $param);
		exit;		
	   }
		
		//Fetch Available Numbers
	    if($this->input->post('slug')=='allocate')
	   {
	   $param = array(
                "Operator_id" => $this->input->post('operator'),
				"country_id" => $this->input->post('country'),
				"city_id" => $this->input->post('city'),
				"prefix_id" => $this->input->post('prefix'),
				"count" => $this->input->post('count'),
				"Allocatedby" => $this->input->post('Allocatedby')
            );

		//print_r($param);			
		echo $num_range = apinewPost(config_item('number_pool_url'). "GetAllocatedStartRangeEndRange", $param);		
		exit;		
	   }
	   
	   //Fetch Available Numbers
	    if($this->input->post('slug')=='assign')
	   {
	   $param = array(
                "service_id" => $this->input->post('service'),
				"id" => $this->input->post('id'),
				"count" => $this->input->post('count'),
				"status" => 1,
				"Allocatedby" => "Admin"
            );	
		//print_r($param);
		echo $assigned = apinewPost(config_item('number_pool_url'). "UpdateServiceIdStatus", $param);		
		exit;		
	   }
	   endif;
	  $data['activeTab'] = "number_pool";
	  $this->load->view("allocate_number",$data);
	}
	
	public function service_details($redirect_to = '') {
		$data['activeTab'] = "utilized";
		$prefix = urldecode(end($this->uri->segment_array()));		
		$city = count($this->uri->segment_array())-1;	
		$service_name = count($this->uri->segment_array())-2;
		$country = count($this->uri->segment_array())-3;
		$city = urldecode($this->uri->segment($city));		
		$country = urldecode($this->uri->segment($country));
		$service_name = urldecode($this->uri->segment($service_name));
		if($city == "NA")
		{$city = "";}
		$param = array(
		"country" => trim($country),
		"service_name" => trim($service_name),
		"city_name" => trim($city),
		"prefixcode" => trim($prefix)
		);
		
		if($service_name == "LLOM")
		{
		$data['service_name'] ="LLOM"; 
		$api_name = "GetLLOMExtnNo";
		}
		else if($service_name == "SIP Trunk External Numbers")
		{
		$data['service_name'] ="SIP Trunk External Numbers"; 			
		$api_name = "GetSipTrunkExtenDtl";
		}
		else if($service_name == "Unified Ring External Numbers")
		{
		$data['service_name'] ="Unified Ring External Numbers"; 
		$api_name = "GetUnifiedringExtnoDtl";
		}
		else if($service_name == "Chilly Talk Access Numbers")
		{
		$data['service_name'] ="Chilly Talk Access Numbers"; 
		$api_name = "GetChillytalkDtls";
		}
		else
		{
		$data['service_name'] ="LLOM"; 
		$api_name = "GetLLOMExtnNo";
		}
		//print_r($param);
		//echo $api_name;
		$data['services_list'] = apiPost(config_item('number_pool_url'). $api_name, $param);
		//echo "<pre>";
		//print_r($data['services_list']);
		$this->load->view("service_details",$data);
	}	
	
	public function add_operator(){
		$Operator_Name = $this->input->post('op');
		$country_name = '';
		$param = array(
		"Operator_Name" => $Operator_Name,
		"country" => $country_name
		);
		$get_result = apiPost(config_item('number_pool_url')."AddNewOperator", $param);
		if($get_result->errcode == 0)
		echo 0;
		else
		echo $get_result->errmsg;
	}
	
	public function recycle_number(){
		$service = $this->input->post('service');
		$id = $this->input->post('id');
		$service_id = $this->input->post('service_id');
		if($service_id == 2)
		{
		$exp = explode(",",$id);
		$param = array(
		"from_range" => $exp[0],
		"to_range " => $exp[1],
		"service_id" => $service_id
		);
		}
		else
		{
		$param = array(
		"id" => $id,
		"service_id" => $service_id
		);
		}
		//print_r($service);
		//exit;
		//echo apiPost(config_item('number_pool_url').$service);
		echo $get_result = apinewPost(config_item('number_pool_url').$service, $param);		
		
	}
	
	public function prefix($redirect_to = '') {
		$data['number_list'] = apiPost(config_item('number_pool_url'). "GetPrefixDetails", '');	
		 if($this->input->post('slug')=='city_prefix')
	   {
	   $param = array(
                "id" => $this->input->post('city_prefix')
            );	
		//print_r($param);
		echo $data['all_details'] = apinewPost(config_item('number_pool_url'). "GetPrefixDtlsByPrefix", $param);		
		exit;		
	   }
		//if($data['utilized_list'][0]->errcode !=0)
		//{
		//	$data = "";
		//}city_prefix
		 $data['country_output'] = apiPost(config_item('number_pool_url'). "GetOperatorAndCountry", '');
		$data['activeTab'] = "prefix";
		$this->load->view("prefix",$data);
	}
	
	public function add_prefix($redirect_to = '') {
	  if ($_SERVER['REQUEST_METHOD'] === 'POST'):
	  if($this->input->post('id') == '')
	  {
            $data = array(
                "country" => $this->input->post('country'),
				"country_id" => $this->input->post('country_prefix'),
				"city" => $this->input->post('city_name'),
				"city_prefix" => $this->input->post('city_prefix')				
            );
            $response = apinewPost(config_item('number_pool_url')."InsertPrefixDtls", $data);  
	}
	else
	{
			 $data = array(
				"id" => $this->input->post('id'),
                "country" => $this->input->post('country'),
				"country_id" => $this->input->post('country_prefix'),
				"city" => $this->input->post('city_name'),
				"city_prefix" => $this->input->post('city_prefix')				
            );
            $response = apinewPost(config_item('number_pool_url')."UpdatePrefixDtls", $data); 
	}
			echo $response;
			exit;		
        endif;
	}

	public function logout(){
		session_destroy();
		unset($_SESSION);		
		$this->session->sess_destroy();
		redirect(site_url('/'));
	}

}


