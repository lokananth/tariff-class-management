<?php class user_model extends CI_Model {

	      var $login = "login";
	      var $performance_supervisors = 'performance_supervisors';
	      var $performance_user_mapping = 'performance_user_mapping';
	      var $email_authentication = "email_authentication";
	      var $account_activity = "account_activity";
	      var $leave_opening_balance = "leave_opening_balance";

	      function __construct() {
	      parent::__construct();
    }

    function login_verification($employee_id,$password){
	      $sql = "SELECT * FROM login where employee_id='".$employee_id."' and password='".md5($password)."' and is_active=1";
	      $query = $this->db->query($sql);
	      return $query->result_array();
    }
    function checkcurrentpassword_user($password,$curr_userid){
	      $sql = "SELECT * FROM login where password='".md5($password)."' AND id=".$curr_userid;
	      $query = $this->db->query($sql);
	      return $query->result_array();
    }
    
    function updateUser($update){
	      $this->db->where("id",$update['id']);
	      $this->db->update($this->login,$update);
    }
    
    function addLeaveBalance($data){ 
	      $this->db->update('leave_opening_balance', $data);
    }
   
    function reportTo($userId){
	      $sql = "select supervisor_id from login where id = ".$userId;
	      $query = $this->db->query($sql);
	      return $query->result_array();  
    }
    
    function updatepassword_user($password,$id){
	      $sql ="UPDATE login SET password='".$password."' WHERE id=".$id;
	      $query = $this->db->query($sql);
    }

    function checkusername($username){
	      $sql = "SELECT * FROM login where employee_id='".$username."'";
	      $query = $this->db->query($sql);
	      return $query->result_array();
    }
    
    function checkemailId($email){
	      $sql = "SELECT * FROM login where email_id='".$email."'";
	      $query = $this->db->query($sql);
	      return $query->result_array();
    }

    function newuser_insertion($data) {
	      $query = $this->db->insert($this->login, $data);
	      $id = $this->db->insert_id();
	      $queryrun = "INSERT INTO md_employee_submitted_data(id,employee_id,firstname,lastname,name,official_mail_id,role,status) (select id,employee_id,firstname,lastname,username,email_id,role,'Open' from login where employee_id NOT IN(select employee_id from md_employee_submitted_data))";
	      $query = $this->db->query($queryrun);
	      return $id; 
        }
    function check_Employee($empId){
	      $sql = "select * from login where employee_id =" .$empId;
	      $query = $this->db->query($sql);
	      return $query->result_array();
    }    


    function count_userlist($filter,$descending,$name){
	      $active = 1;  
	      if($name != ""){
			if(($name != "") && ($filter == 'unchecked') && ($descending == 'unchecked')){
			    $sql = "select * from login where firstname like '".$name."%' or lastname like '".$name."%' and is_active=".$active. ' order by employee_id asc';
			}elseif(($name != "") && ($filter == 'checked') && ($descending == 'unchecked')){
			    $sql = "select * from login where firstname like '".$name."%' or lastname like '".$name."%' order by employee_id asc";
			}elseif(($name != "") && ($filter == 'unchecked') && ($descending == 'checked')){
			    $sql = "select * from login where firstname like '".$name."%' or lastname like '".$name."%' and is_active=".$active.' order by employee_id desc';
			}else{
			    $sql = "select * from login where firstname like '".$name."%' or lastname like '".$name."%' order by employee_id desc";
			}
			$query = $this->db->query($sql);
			return $query->num_rows();
	      }else{
			if(($name == "") && ($filter == 'unchecked') && ($descending == 'unchecked')){
			    $sql ="select * from login where is_active=".$active. ' order by employee_id asc ';
			}elseif(($name == "") && ($descending == 'checked') && ($filter == 'unchecked')){
			    $sql ="select * from login where is_active=".$active. ' order by employee_id desc ';
			}elseif(($name == "") && ($filter == 'checked') && ($descending == 'unchecked')){
			    $sql = "SELECT * FROM login order by employee_id asc";
			}elseif(($name == "") && ($filter == 'checked') && ($descending == 'checked')){
			    $sql = "SELECT * FROM login order by employee_id desc";
			}else{
			    $sql ="select * from login where is_active=".$active. ' order by employee_id asc ';
			}
			$query = $this->db->query($sql);
			return $query->num_rows();
		  }
 
  }
  
    function userlist($limit,$start,$filter,$descending,$name){
		$active = 1;
		if($name != ""){
		    if(($name != "") && ($filter == 'unchecked') && ($descending == 'unchecked')){
			$sql = "select * from login where firstname like '".$name."%' or lastname like '".$name."%' and is_active=".$active. ' order by employee_id asc';
		    }elseif(($name != "") && ($filter == 'checked') && ($descending == 'unchecked')){
			$sql = "select * from login where firstname like '".$name."%' or lastname like '".$name."%' order by employee_id asc";
		    }elseif(($name != "") && ($filter == 'unchecked') && ($descending == 'checked')){
			$sql = "select * from login where firstname like '".$name."%' or lastname like '".$name."%' and is_active=".$active.' order by employee_id desc';
		    }else{
			$sql = "select * from login where firstname like '".$name."%' or lastname like '".$name."%' order by employee_id desc";
		    }
		    
		    $sql .= "  limit ".$start.",".$limit;
		    $query = $this->db->query($sql);
		    return $query->result_array();
		}else {
		    if(($name == "") && ($filter == 'unchecked') && ($descending == 'unchecked')){
			$sql ="select * from login where is_active=".$active. ' order by employee_id asc ';
		    }elseif(($name == "") && ($descending == 'checked') && ($filter == 'unchecked')){
			$sql ="select * from login where is_active=".$active. ' order by employee_id desc ';
		    }elseif(($name == "") && ($filter == 'checked') && ($descending == 'unchecked')){
			$sql = "SELECT * FROM login order by employee_id asc";
		    }elseif(($name == "") && ($filter == 'checked') && ($descending == 'checked')){
			$sql = "SELECT * FROM login order by employee_id desc";
		    }else{
			$sql ="select * from login where is_active=".$active. ' order by employee_id asc ';
		    }
		    $sql .= "  limit ".$start.",".$limit;
		    $query = $this->db->query($sql);
		    return $query->result_array();
		}
    }
  
  function updateemail_user($email,$id){
		$sql ="UPDATE login SET email_id='".$email."' WHERE id=".$id;
		$query = $this->db->query($sql);
    }

    function update_userrole($role,$id){
		$sql ="UPDATE login SET role='".$role."' WHERE id=".$id;
		$query = $this->db->query($sql);
    }

    function update_userrole_type($role_type,$id){
		$sql ="UPDATE login SET role_type='".$role_type."' WHERE id=".$id;
		$query = $this->db->query($sql);
     }
  
    function getIdbyemployee($report_too){
		$sql =" select id from login where employee_id = '" .$report_too. "' ";
		$query = $this->db->query($sql);
		return $query->result_array();
    }
    

    function getUserProfile($id) {
		$sql = "SELECT * from login where id='".$id."'";
		$query = $this->db->query($sql);
		return $query->result_array();
    }
     
     
    function getAllEmployee($q,$id,$sid) {
		$sql = "SELECT username,id from login where username like '" .$q."%' and is_active = 1 and role NOT IN('Administrator') ";
		$sql .= " and id != " .$id;
	        if($sid != 0){
		$sql .= " and id !=" .$sid;
	        }
		$query = $this->db->query($sql);
		return $query->result();
      }
   
   
    function updateSupervisorMembers($data){
              $sql = " update login set supervisor_id = " .$data['supervisor_id']. " where id = " .$data['id'];
              $query = $this->db->query($sql);
    }
    function getAllAdminAndSupervisors($notEmpId = 0) {
		$sql ="select employee_id,username,id from login where role IN ('Administrator') or is_supervisor = 1 ";
		$sql .= " and is_active = 1"; 
		if ($notEmpId != 0) {
		    $sql .= " and ( id != " . $notEmpId . ") ";
		}
		$query = $this->db->query($sql);
		return $query->result_array();
    }
      
    function editUserInformation($data,$id){
		$this->db->update('login', $data, array('employee_id' => $id));
    }
    
   
    function checkMappingInformation($id){
		$sql = " select supervisor_id from login where employee_id = ".$id;
		$query = $this->db->query($sql);
		return $query->result_array();
      }

    function getUserEmployeeIdByusername($name) {
		$sql = "SELECT employee_id,username,id from login where username ='" .$name. "'";
		$query = $this->db->query($sql);
		return $query->result_array();
     }
      
    function getSupervisorStatus($uid){
		$sql = " select supervisor_id from login where id =" .$uid ;
		$query = $this->db->query($sql);
		return $query->result_array();
    }
    function getSupervisorFlag($userId){
		$sql =" select is_supervisor from login where id =" .$userId;
		$query = $this->db->query($sql);
		$value = $query->result_array();
		$exits = 0;
	        if($value[0]['is_supervisor'] != 0){
	        $exits = 1;
	        }
	        return $exits;
    } 
   
     function verify_authentication($employee_id) {
		$sql = "SELECT * FROM login WHERE employee_id = '".$employee_id."'";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
    function insertUniqueId($data) {
		$query = $this->db->insert($this->email_authentication, $data);
		return $this->db->insert_id();
	}

    function selectUniqueId($employee_id) {
		$sql = "SELECT * FROM email_authentication where employee_id = '".$employee_id."'";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

    function getUniqueIdEmployee($id) {
		$sql = "SELECT * FROM email_authentication where unique_id='".$id."' AND is_active=1";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

    function updatePassword($employee_id,$password) {
		$sql = "UPDATE login set password='".md5($password)."' WHERE employee_id='".$employee_id."'";
		$query = $this->db->query($sql);
	}

    function updateResetCount($employee_id,$count,$reseted_date,$active_flag) {
		$sql = "UPDATE email_authentication set reset_count = '".$count."',reseted_date='".$reseted_date."',is_active='".$active_flag."' WHERE employee_id = '".$employee_id."'";
		$query = $this->db->query($sql);
	}

    function updateUniqueId($data,$id) {
		$this->db->update('email_authentication', $data, array('employee_id' => $id));
	}
    function insertMembersMappings($superRowId,$userId){
		$sql = " update login set supervisor_id = " .$superRowId. " where id = " . $userId;
		$query = $this->db->query($sql);
		return $this->db->insert_id();
	}

	
    function getSupervisorId($id){
		$sql = " select employee_id from login where id = " .$id. " and is_supervisor = 1 and is_active = 1 ";	
		$query = $this->db->query($sql);
		return $query->result_array();
      }
       
    function deleteUserInfo($id){
	
		$sql="DELETE FROM login where id=".$id;
		$query = $this->db->query($sql);
      }
      
    function deleteMappingInformation($id){
		$sql = "DELETE FROM  performance_user_mapping WHERE supervisor_id =".$id ;
		$query = $this->db->query($sql);
      }
  
    function getSupervisorMembers($id){
		$sql = " select * from login where supervisor_id = " .$id. " and is_active = 1 ";
		$query = $this->db->query($sql);
		return $query->result_array();
      }
       
    function getProjectAdmins($employee_id){
		$sql = " select * from login where is_project_admin = '1' and employee_id =" .$employee_id;
		$query = $this->db->query($sql);
		$val = $query->result_array();
		$isExist;
		if($val){
		$isExist = 1;
		}else{
		$isExist = 0;
		}
		return $isExist;
      }
      
      
    function checkActivity($id){
		$sql = "SELECT * FROM account_activity where employee_id = '".$id."'";
		$query = $this->db->query($sql);
		return $query->result_array();
      }
    function getLostLogin($id){
		$sql = "SELECT DATE_FORMAT(last_login,'%d-%m-%Y %h:%i %p')last_login FROM account_activity where employee_id = '".$id."'";
		$query = $this->db->query($sql);
		return $query->result_array();
      }
    function insertActivity($data){
		$query = $this->db->insert($this->account_activity, $data);
		return $this->db->insert_id();
	}
    function updateActivity($data,$id){
		$this->db->update('account_activity', $data, array('employee_id' => $id));
	}
	
    function getNewEmployeeId(){
	 $sql = " SELECT max(convert(employee_id, unsigned)) as id FROM login";
	  $query = $this->db->query($sql);
	  $rows = $query->result_array();
	  $id = 0;
	  if ($rows) {
	    $id = (int) $rows[0]['id'];
	  }
	  $id++;
	  return ($id);
    }
    
    function getRowsByEmailid($email){
	$sql = "SELECT * FROM login where email_id='".$email."' and is_active=1";
	$query = $this->db->query($sql);
	return $query->result_array();
    }
    
}

?>