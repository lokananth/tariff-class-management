<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class session_check_model extends CI_Model {

	  function __construct() {
	  parent::__construct();
	  if (!isset($_SESSION) ){ 	
		session_start();
		}
	  ini_set('session.gc_maxlifetime', 0);
	}
	
	function session_check(){
// 	ini_set('session.gc_maxlifetime', 60 * 60 * 6);	
	      //ini_set('session.gc_maxlifetime', 60 * 60 * 6);
	      //ini_set('session.gc_maxlifetime', 43200);
	      ini_set('session.gc_maxlifetime', 0);
	      if (!isset($_SESSION) ){ 
		 session_start();
	      } 
	      $employee_id = '';
	      $userName = '';
	      $role = '';
	      if(isset($_SESSION['employee_id'])){
		$employee_id = $_SESSION['employee_id'];
	      }
	      
	      if(isset($_SESSION['username'])){
		$userName = $_SESSION['username'];
	      }
	      
	      if(isset($_SESSION['role'])){
		$role = $_SESSION['role'];
	      }
	       $eid = 0;
	       $id = 0;
	       $role = '';
	      if ((isset($this->session->userdata['employee_id'])) || (isset($this->session->userdata['id'])) || (isset($this->session->userdata['role']))) {
		    $eid = (int) $this->session->userdata['employee_id'];
		    $id = (int) $this->session->userdata['id'];
		    $role = $this->session->userdata['role'];
		}
	      if(($eid == 0) || ($id == 0)|| ($role == '')) {
		session_destroy();
		redirect(site_url('/'));
		return;
	      }
	         
	      if(($employee_id == '') || ($userName == '') || ($role == '')){
		redirect(site_url('/'));
	      }
	      
	}
	
	
}

?>