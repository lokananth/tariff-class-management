<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		EllisLab Dev Team
 * @copyright		Copyright (c) 2008 - 2014, EllisLab, Inc.
 * @copyright		Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter URL Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		EllisLab Dev Team
 * @link		http://codeigniter.com/user_guide/helpers/api_helper.html
 */
 
function apiGet($apiUrl) {
    $headers = array(
        'Authorization: mundiovectone z0ejtXAoY2BpLuwqu6KHYjHREK6ll60vFNt81YM5sx8=',
		'Mundio-Api-PublicKey: OTE3MjAwMDIyNTM4',
		'Content-MD5: 917200022538',
		'User-Agent: mundiovectone',
		'Accept: application/json',
		'Content-Type: application/json',
		'Host: 192.168.2.102:9011',
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $apiUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_USERAGENT, 'geoPlugin PHP Class v1.0');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//                curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
    $response = curl_exec($ch);
    if ($response === FALSE) {
        $response = htmlspecialchars(curl_errno($ch));
    }
    curl_close($ch);
    return json_decode($response);
}

function apiPost($apiUrl, $data) {

//print_r($apiUrl);

// set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $apiUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: mundiovectone z0ejtXAoY2BpLuwqu6KHYjHREK6ll60vFNt81YM5sx8=',
		'Mundio-Api-PublicKey: OTE3MjAwMDIyNTM4',
		'Content-MD5: 917200022538',
		'User-Agent: mundiovectone',
		'Accept: application/json',
		'Content-Type: application/json',
		'Host: 192.168.2.102:9011',
    ));
// execute the request
    $output = curl_exec($ch);
    return json_decode($output);
}

function apinewPost($apiUrl, $data) {

//print_r($apiUrl);

// set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $apiUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: mundiovectone z0ejtXAoY2BpLuwqu6KHYjHREK6ll60vFNt81YM5sx8=',
		'Mundio-Api-PublicKey: OTE3MjAwMDIyNTM4',
		'Content-MD5: 917200022538',
		'User-Agent: mundiovectone',
		'Accept: application/json',
		'Content-Type: application/json',
		'Host: 192.168.2.102:9011',
    ));
// execute the request
    $output = curl_exec($ch);
    return $output;
}

/* End of file api_helper.php */
/* Location: ./system/helpers/api_helper.php */