<?php 
require_once "swift/swift_required.php";

	function sendmail_information($subject,$from,$to,$message,$emailcc,$hasattachments){
	     if(empty($from) || (empty($to)) ){
		return false;
	    }
	    $transport = Swift_SmtpTransport::newInstance();
	    $swiftMessage = Swift_Message::newInstance();
	    $swiftMailer = Swift_Mailer::newInstance($transport);
	    $swiftMessage->setSubject($subject);
	    $swiftMessage->setFrom($from);
	    $swiftMessage->setTo($to);
	    if(!empty($emailcc)){
	      $swiftMessage->setCc($emailcc);
	    }
	    if(!empty($hasattachments)){
	      foreach($hasattachments as $values){
		$filename = $values;
		$file = getcwd()."/uploads/".$filename;
		if (strpos($filename, '.s3.') > -1) {
		  $file = $filename;
		}
		$swiftMessage->attach(Swift_Attachment::fromPath($file));
	      }
	    }
// 	    $swiftMessage->setBcc();
	    $swiftMessage->addPart('<div>'.nl2br($message) . '</div>', 'text/html');
	    $sent_flag = $swiftMailer->send($swiftMessage);

	    if($sent_flag){
	      return TRUE;
	    }
	  }
	  
	  function offer($subject,$from,$to,$message,$emailcc,$hasattachments){
	      

	  }
	  
	  function sendmail_information_status($subject,$from,$to,$message,$emailcc,$hasattachments){
	     if(empty($from) || (empty($to)) ){
		return false;
	    }
	    $transport = Swift_SmtpTransport::newInstance();
	    $swiftMessage = Swift_Message::newInstance();
	    $swiftMailer = Swift_Mailer::newInstance($transport);
	    $swiftMessage->setSubject($subject);
	    $swiftMessage->setFrom($from);
	    $swiftMessage->setTo($to);
	    if(!empty($emailcc)){
	      $swiftMessage->setCc($emailcc);
	    }
	    if(!empty($hasattachments)){
	      foreach($hasattachments as $values){
		$filename = $values;
		$file = getcwd()."/attachements/".$filename;
		$swiftMessage->attach(Swift_Attachment::fromPath($file));
	      }
	    }
// 	    $swiftMessage->setBcc();
	    $swiftMessage->addPart('<div>'.nl2br($message) . '</div>', 'text/html');
	    $sent_flag = $swiftMailer->send($swiftMessage);
	    if($sent_flag){
	      return TRUE;
	    }

     }
	  
	  
	  function offermail($subject,$from,$to,$message,$emailcc,$hasattachments){
	
	    $transport = Swift_SmtpTransport::newInstance();
	    $swiftMessage = Swift_Message::newInstance();
	    $swiftMailer = Swift_Mailer::newInstance($transport);
	    $swiftMessage->setSubject($subject);
	    $swiftMessage->setFrom($from);
	    $swiftMessage->setTo($to);
	    if(!empty($emailcc)){
	      $swiftMessage->setCc($emailcc);
	    }
		$filename = $hasattachments;
		$file = getcwd()."/uploads/offerLetter/".$filename;
		$swiftMessage->attach(Swift_Attachment::fromPath($file));

// 	    $swiftMessage->setBcc();
	    $swiftMessage->addPart('<div>'.nl2br($message) . '</div>', 'text/html');
	    $sent_flag = $swiftMailer->send($swiftMessage);

	    if($sent_flag){
	      return TRUE;
	    }
	    
	    }
	  
     

