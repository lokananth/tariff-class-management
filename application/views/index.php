<?php include('header.php');?>
		  
		<!-- MAIN PANEL -->
		<div id="main" role="main">
	<!-- MAIN CONTENT -->
			<div id="content">

				<!-- row -->
				<div class="row"> 
					 <div id="content" class="col-md-offset-3 col-md-5 full-page login">
				  
							<form action="<?php echo base_url(); ?>user/login_form_submission" id="login-form" method="POST" class="smart-form client-form">
							 <img src="<?php echo base_url(); ?>images/logo-mundio.png" alt class="logo"> 
								<header>
									<b>Tariff Class Management System</b>
								</header>
								<div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
								<?php if(isset($login_error)){ ?>
								<?php                                
								echo '<div class="al alert alert-warning">
								<a href="#" class="close" data-dismiss="alert">&times;</a>
								'.$login_error.'
								</div>';
								?>
								<?php }?>
								<fieldset> 
									<section>
										<label class="label">E-mail</label>
										<label class="input"> <i class="icon-append fa fa-user"></i>
											<input required type="email" name="email">
											<b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Please enter email address/username</b></label>
									</section>
									<section>
										<label class="label">Password</label>
										<label class="input"> <i class="icon-append fa fa-lock"></i>
											<input required type="password" name="password">
											<b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Enter your password</b> </label>
										<!-- <div class="note">
											<a href="forgotpassword.html">Forgot password?</a>
										</div> -->
									</section> 
								</fieldset>
								<footer>
									<button type="submit" class="btn btn-primary">
										Sign in
									</button>
								</footer>
							</form>

						</div>
							 
				
						</div>
				
					</div>

				<!-- end row -->

			</div>
			
			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->
<?php include('footer.php');?>
