<?php include('header.php');?>
<?php include('inner-menu.php');?>
<?php include('left-menu.php');?>
		<!-- MAIN PANEL -->
		<div id="main" role="main">

		<!-- MAIN CONTENT -->
			<div id="content">

				<div class="row">
					<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
						<h3 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-home"></i> TARIFF CLASS </h3><!-- <span>&nbsp;>&nbsp; Device List</span> -->
					</div> 
				</div>
				<!-- widget grid -->
				<section id="widget-grid" class="">
				
					<!-- row -->
					<div class="row">
				
						<!-- NEW WIDGET START -->
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
							<!-- Widget ID (each widget will need unique ID)-->
							
							<!-- end widget -->
				
							<!-- Widget ID (each widget will need unique ID)-->
							<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false" data-widget-editbutton="false"  data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" >
								
								
								<!-- widget div-->
								<div>
				
									<!-- widget edit box -->
								
									<!-- end widget edit box -->
				
									<!-- widget content -->
									<div class="widget-body no-padding">								
									
										<div class="col-sm-12">
											<!--<button type="button" class="btn btn-default add"  data-toggle="modal" data-target="#myModal">Add Device</button>-->
											<!-- <button type="button" class="btn btn-default add"  data-toggle="modal" data-target="#myModal2">Update Products</button>	 -->
										</div>
										<?php //echo "<pre>";print_r($number_list);exit;
										$number_list = isset($number_list) ? $number_list : Array();						
										?>
										<?php if(isset($number_list)):
										if($number_list[0]->errcode == 0):?>
										<table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
											<thead>
												<tr role="row">
												<th  role="columnheader">Tariff Class</th>
												<th  role="columnheader">Type</th>
												<th  role="columnheader">Country</th>
												<th  role="columnheader">Brand</th>
												<th  role="columnheader">Sim Type</th>                                        
												<th  role="columnheader">Class Type</th>
												<th  role="columnheader">Roaming Tariff</th>              
												<th  role="columnheader">Cost</th>
												<th  role="columnheader">Card Name</th>                                        
												<th  role="columnheader">Validity</th>
												<th  role="columnheader">Mode</th>
												</tr>
												<tr>
													<th data-hide="phone">Tariff Class</th>
													<th data-class="expand">Type</th>
													<th>Country</th>
													<th data-hide="phone">Brand</th>
													<th data-hide="phone,tablet">Sim Type</th>
													<th data-hide="phone,tablet">Class Type</th>
													<th data-hide="phone,tablet">Roaming Tariff</th>
													<th data-hide="phone,tablet">Cost</th>
													<th data-hide="phone,tablet">Card Name</th>
													<th data-hide="phone,tablet">Validity</th>
													<th data-hide="phone,tablet">Mode</th>
													
												</tr>
											</thead>
											<tbody>
												<?php foreach($number_list as $list):  ?>
												<tr>												
												<td><a  data-toggle="modal" data-target="#myModal2"><?php echo $list->Operator_name; ?></a></td>
												<td><?php echo $list->Country; ?></td>
												<td><?php echo $list->City; ?></td>
												<td><?php echo $list->Prefix_code; ?></td>
												<td><?php echo $list->From_Range; ?></td>
												<td><?php echo $list->To_Range; ?></td>
												<td><?php echo $list->No_Count; ?></td>
												<td><?php echo $list->ServiceName; ?></td>
												<td><?php echo $list->Allocateddate; ?></td>
												<td><?php echo $list->AllocatedBy; ?></td>
												<td><?php echo $list->AllocatedBy; ?></td>													
												</tr>
												<?php endforeach; ?>
												
											</tbody>
										</table>
									<?php else: ?>
								<div class="panel panel-default text-center bold-border m-t-20 Sip-pnl">
									<div class="panel-body">
										No Records Found.
									</div>
								</div>
						<?php endif;?>
						<?php else: ?>
								<div class="panel panel-default text-center bold-border m-t-20 Sip-pnl">
									<div class="panel-body">
										No Records Found.
									</div>
								</div>

						<?php endif;?>
									</div>
									<!-- end widget content -->
				
								</div>
								<!-- end widget div -->
				
							</div>
							<!-- end widget -->
							
				<!-- modal-->
				 <div class="modal fade" id="myModal2" tabindex="-2" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									&times;
								</button>
								<h4 class="modal-title" id="myModalLabel">Device Details</h4>
							</div>
							<div class="modal-body">
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label class="col-sm-4" for="tags">Country</label>
											<span class="help-inline">UK</span>
										</div>
										<div class="form-group">
											<label class="col-sm-4" for="tags">Device Name</label>
											<span class="help-inline">Dell</span>
										</div>
										<div class="form-group">
											<label class="col-sm-4" for="tags">Device ID</label>
											<span class="help-inline">as123d</span>
										</div>										
										<div class="form-group">
											<label class="col-sm-4" for="tags">Device Type</label>
											<span class="help-inline">Storage</span>
										</div>
										<div class="form-group">
											<label class="col-sm-4" for="tags">Device Description</label>
											<span class="help-inline">Description</span>
										</div>
										<div class="form-group">
											<label class="col-sm-4" for="tags">Added On</label>
											<span class="help-inline">04-01-2016</span>
										</div>
									</div>	
								</div>
								<div class="row">
									<div class="col-md-12 text-center">
										<button type="button" class="btn btn-default" >
											Edit
										</button>
										<button type="button" class="btn btn-primary" data-dismiss="modal">
											cancel
										</button>
									</div>
								</div>
								<div class="row m-t-20">
									<div class="col-md-12">
										<table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
											<thead>
												<tr>
													<th>Issue ID</th>
													<th>Raised Date</th>
													<th>Clarification</th>
													
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>1023</td>
													<td>05-01-2016</td>
													<td>device failure</td>
												</tr>
												<tr>
													<td>1023</td>
													<td>05-01-2016</td>
													<td>device failure</td>
												</tr>
												<tr>
													<td>1023</td>
													<td>05-01-2016</td>
													<td>device failure</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div>
						

	<!-- modal end-->		
	
					<!-- modal-->
				 <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									&times;
								</button>
								<h4 class="modal-title" id="myModalLabel">Add Device</h4>
							</div>
							<div class="modal-body">
								<div class="row">
									<form class="form-horizontal" action="">
										<div class="col-md-12">
											<div class="form-group">
												<label class="col-sm-4 control-label" for="tags">Country</label>
												<div class="col-sm-6">
													<select class="form-control">
														<option class="selected">Select Country</option>
														<option>UK</option>
														<option>AT</option>
														<option>PT</option>
														<option>SE</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-4 control-label" for="tags">Device Name</label>
												<div class="col-sm-6">
													<input type="text" class="form-control" value=""  placeholder="Device Name" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-4 control-label" for="tags">Device ID</label>
												<div class="col-sm-6">
													<input type="text" class="form-control" value="" placeholder="Device ID" />
												</div>
											</div>										
											<div class="form-group">
												<label class="col-sm-4 control-label" for="tags">Device Type</label>
												<div class="col-sm-6">
													<input type="text" class="form-control" value=""  placeholder="Device Type" />
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-4 control-label" for="tags">Device Description</label>
												<div class="col-sm-6">
													<textarea class="form-control" placeholder="Description"></textarea>
												</div>
											</div>
										</div>	
									</form>
								</div>
								<div class="row">
									<div class="col-md-12 text-center">
										<button type="button" class="btn btn-default" >
											OK
										</button>
										<button type="button" class="btn btn-primary" data-dismiss="modal">
											cancel
										</button>
									</div>
								</div>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div>
						

	<!-- modal end-->		
	<!-- modal-->
				 <div class="modal fade" id="myModal3" tabindex="-2" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									&times;
								</button>
								<h4 class="modal-title" id="myModalLabel">Conformation</h4>
							</div>
							<div class="modal-body dsp-block" >
								<div class="col-sm-12">
									<div class="row">			
										<strong class="text-center">Do  you want to mark this device as retired</strong>
									</div>
								</div>
							</div>
							<div class="modal-footer text-center">
								<button type="button" class="btn btn-default" >
									OK
								</button>
								<button type="button" class="btn btn-primary" data-dismiss="modal">
									CANCEL
								</button>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div>
	<!-- modal end-->	
	
						</article>
						<!-- WIDGET END -->
				
					</div>
				
					<!-- end row -->
				
					<!-- end row -->
				
				</section>
				<!-- end widget grid -->

			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->
<?php include('footer.php');?>

  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <script>
  $(function() {
    var spinner = $( ".spinner" ).spinner();
    $( "button" ).button();
  });
  </script>