update md_employee_submitted_data set status = 'Open' where status = 'NotSubmitted';

update md_employee_submitted_data set status = admin_status where status = 'Submitted';

update md_employee_submitted_data set status = 'Submitted' where status = 'Pending';

alter table md_employee_submitted_data drop admin_status;
