

CREATE TABLE IF NOT EXISTS `md_personal_details_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(50) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `employee_id` varchar(10) NOT NULL,
  `date_of_joining` datetime NOT NULL,
  `date_of_birth` datetime NOT NULL,
  `age` int(3) NOT NULL,
  `birth_place` varchar(255) NOT NULL,
  `blood_group` varchar(10) NOT NULL, 
  `personal_emailid` varchar(255) NOT NULL, 
  `mobile_number` varchar(13) NOT NULL,
  `martial_status` varchar(10) NOT NULL,
  `pan_number` varchar(10) NOT NULL,
  `nationality` varchar(20) NOT NULL,
  `identification_mark` text NOT NULL,
  `hobbies` text NOT NULL,
  `updated_date` datetime NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT = 1 ;


CREATE TABLE IF NOT EXISTS `md_passport_details_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `employee_id` varchar(10) NOT NULL,
  `passport_number` varchar(255) NOT NULL, 
  `place_of_issue` varchar(255) NOT NULL, 
  `date_of_issue` date NOT NULL,
  `date_of_expiry` date NOT NULL,
  `updated_date` datetime NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT = 1 ;


CREATE TABLE IF NOT EXISTS `md_education_details_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `employee_id` varchar(10) NOT NULL,
  `graduation` varchar(255) NOT NULL, 
  `specialization` varchar(255) NOT NULL, 
  `grade_attained` varchar(255) NOT NULL,
  `university` varchar(255) NOT NULL,
  `year_of_completion` int(5) NOT NULL,
  `updated_date` datetime NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT = 1 ;


CREATE TABLE IF NOT EXISTS `md_experience_details_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `employee_id` varchar(10) NOT NULL,
  `designation` varchar(255) NOT NULL, 
  `department_function` varchar(255) NOT NULL, 
  `company` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `tenure` varchar(255) NOT NULL,
  `updated_date` datetime NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT = 1 ;




CREATE TABLE IF NOT EXISTS `md_skillset_details_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `employee_id` varchar(10) NOT NULL,
  `area` varchar(255) NOT NULL, 
  `skills_acquired` varchar(255) NOT NULL, 
  `certificate` varchar(255) NOT NULL,
  `project_summary` text NOT NULL,
  `updated_date` datetime NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT = 1 ;



CREATE TABLE IF NOT EXISTS `md_local_address_details_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `employee_id` varchar(10) NOT NULL,
  `local_door_number` varchar(255) NOT NULL, 
  `local_street` varchar(255) NOT NULL, 
  `local_location` varchar(255) NOT NULL,
 `local_city` varchar(20) NOT NULL,
 `local_district` varchar(20) NOT NULL,
 `local_state` varchar(20) NOT NULL,
  `local_country` varchar(20) NOT NULL,
  `local_pincode` varchar(10) NOT NULL,
  `local_contact_no` varchar(13) NOT NULL,
  `updated_date` datetime NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT = 1 ;



CREATE TABLE IF NOT EXISTS `md_permanent_address_details_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `employee_id` varchar(10) NOT NULL,
  `permanent_door_number` varchar(255) NOT NULL, 
  `permanent_street` varchar(255) NOT NULL, 
  `permanent_location` varchar(255) NOT NULL,
 `permanent_city` varchar(20) NOT NULL,
 `permanent_district` varchar(20) NOT NULL,
 `permanent_state` varchar(20) NOT NULL,
  `permanent_country` varchar(20) NOT NULL,
  `permanent_pincode` varchar(10) NOT NULL,
  `permanent_contact_no` varchar(13) NOT NULL,
  `updated_date` datetime NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT = 1 ;



CREATE TABLE IF NOT EXISTS `md_family_details_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `employee_id` varchar(10) NOT NULL,
  `name` varchar(25) NOT NULL, 
  `relation` varchar(25) NOT NULL, 
  `date_of_birth` date NOT NULL,
 `occupation` varchar(25) NOT NULL,
 `educational_status` varchar(25) NOT NULL,
 `updated_date` datetime NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT = 1 ;




CREATE TABLE IF NOT EXISTS `md_certificate_details_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `employee_id` varchar(10) NOT NULL,
  `offer_letter` tinyint(1) NOT NULL, 
  `photo` tinyint(1) NOT NULL, 
  `relieving_letter` tinyint(1) NOT NULL,
 `salary_certificate` tinyint(1) NOT NULL,
 `educational_status` tinyint(1) NOT NULL,
 `passport_details` tinyint(1) NOT NULL,
 `updated_date` datetime NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT = 1 ;


