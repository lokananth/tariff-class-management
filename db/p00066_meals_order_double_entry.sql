

DELETE e1 FROM meals_order e1, meals_order e2 WHERE e1.employee_id = e2.employee_id AND e1.start_date=e2.start_date AND e1.end_date=e2.end_date AND e1.meals_order_id > e2.meals_order_id;

DELETE e1 FROM meals_order_details e1, meals_order_details e2 WHERE e1.meals_order_details_id > e2.meals_order_details_id AND e1.meals_order_id > e2.meals_order_id AND e1.employee_id=e2.employee_id AND e1.date=e2.date AND e1.meals_type=e2.meals_type AND e1.status=e2.status;
