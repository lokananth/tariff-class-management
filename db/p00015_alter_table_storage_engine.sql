
ALTER TABLE account_activity
ENGINE=InnoDB;


ALTER TABLE authorization
ENGINE=InnoDB;



ALTER TABLE bq_holiday
ENGINE=InnoDB;


ALTER TABLE cb_list_details
ENGINE=InnoDB;


ALTER TABLE cb_type_details
ENGINE=InnoDB;


ALTER TABLE compoff_details
ENGINE=InnoDB;


ALTER TABLE compoff_form
ENGINE=InnoDB;



ALTER TABLE email_authentication
ENGINE=InnoDB;



ALTER TABLE employee_leave
ENGINE=InnoDB;



ALTER TABLE form_no
ENGINE=InnoDB;


ALTER TABLE general_feedback
ENGINE=InnoDB;


ALTER TABLE google_login
ENGINE=InnoDB;



ALTER TABLE leave_opening_balance
ENGINE=InnoDB;



ALTER TABLE leave_transaction
ENGINE=InnoDB;



ALTER TABLE leave_type
ENGINE=InnoDB;



ALTER TABLE login
ENGINE=InnoDB;


ALTER TABLE login_details
ENGINE=InnoDB;

ALTER TABLE md_certificate_details
ENGINE=InnoDB;


ALTER TABLE md_certificate_details_history
ENGINE=InnoDB;


ALTER TABLE md_educational_details
ENGINE=InnoDB;


ALTER TABLE md_educational_details_history
ENGINE=InnoDB;


ALTER TABLE md_employee_submitted_data
ENGINE=InnoDB;



ALTER TABLE md_experience_details
ENGINE=InnoDB;


ALTER TABLE md_experience_details_history
ENGINE=InnoDB;


ALTER TABLE md_family_details
ENGINE=InnoDB;

ALTER TABLE md_family_details_history
ENGINE=InnoDB;


ALTER TABLE md_local_address_details
ENGINE=InnoDB;



ALTER TABLE md_local_address_details_history
ENGINE=InnoDB;

ALTER TABLE md_passport_details
ENGINE=InnoDB;


ALTER TABLE md_passport_details_history
ENGINE=InnoDB;


ALTER TABLE md_permanent_address_details
ENGINE=InnoDB;

ALTER TABLE md_permanent_address_details_history
ENGINE=InnoDB;


ALTER TABLE md_personal_details
ENGINE=InnoDB;


ALTER TABLE md_personal_details_history
ENGINE=InnoDB;


ALTER TABLE md_skillset_details
ENGINE=InnoDB;


ALTER TABLE md_skillset_details_history
ENGINE=InnoDB;



ALTER TABLE performance_mapping
ENGINE=InnoDB;



ALTER TABLE performance_report
ENGINE=InnoDB;



ALTER TABLE performance_reviews
ENGINE=InnoDB;



ALTER TABLE performance_status
ENGINE=InnoDB;



ALTER TABLE performance_supervisors
ENGINE=InnoDB;




ALTER TABLE performance_user_mapping
ENGINE=InnoDB;


ALTER TABLE user_attachment
ENGINE=InnoDB;

