create table menu_list_master(
menu_master_id bigint not null primary key  auto_increment,
name varchar(125) default '',
sort_order int default 0
);

create table menu_list_item(
menu_item_id bigint not null primary key  auto_increment,
menu_master_id bigint default 0,
menu_key varchar(125) default '',
name varchar(125) default '',
sort_order int default 0
);

create table menu_list_user(
menu_list_user_id bigint not null primary key  auto_increment,
menu_item_id bigint not null default 0,
role_id bigint not null default 0,
value int default 0
);

create table menu_list_role(
menu_list_user_id bigint not null primary key  auto_increment,
menu_item_id bigint not null default 0,
role_id bigint not null default 0,
value int default 0
);

alter table login add role_id int default 0;


insert into menu_list_master(menu_master_id, name, sort_order) values (1, 'Users', 10);
insert into menu_list_master(menu_master_id, name, sort_order) values (2, 'Employee Informatin', 20);
insert into menu_list_master(menu_master_id, name, sort_order) values (3, 'My details', 20);
insert into menu_list_master(menu_master_id, name, sort_order) values (4, 'Expenses', 30);
insert into menu_list_master(menu_master_id, name, sort_order) values (5, 'Holiday', 40);
insert into menu_list_master(menu_master_id, name, sort_order) values (6, 'Performance Review', 50);
insert into menu_list_master(menu_master_id, name, sort_order) values (7, 'Leave', 60);
insert into menu_list_master(menu_master_id, name, sort_order) values (8, 'Leave Type', 70);
insert into menu_list_master(menu_master_id, name, sort_order) values (9, 'Comp Off', 80);
insert into menu_list_master(menu_master_id, name, sort_order) values (10, 'Timesheet', 90);
insert into menu_list_master(menu_master_id, name, sort_order) values (11, 'Project Info', 100);
insert into menu_list_master(menu_master_id, name, sort_order) values (12, 'Projects', 110);
insert into menu_list_master(menu_master_id, name, sort_order) values (13, 'Customers', 120);
insert into menu_list_master(menu_master_id, name, sort_order) values (14, 'Activity Lists', 130);
insert into menu_list_master(menu_master_id, name, sort_order) values (15, 'Status Update', 140);
insert into menu_list_master(menu_master_id, name, sort_order) values (16, 'Meals Order', 150);


insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (1, 'MENU_USER', 'Users', 1);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (1, 'MENU_USER_CREATE', 'Create new User', 10);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (1, 'MENU_USER_EDIT', 'Edit', 20);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (1, 'MENU_USER_DELETE', 'Delete', 30);

insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (2, 'MENU_EMPLOYEE_INFORMATION', 'Employee Informatin', 1);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (2, 'MENU_EMPLOYEE_INFORMATION_VIEW', 'View', 40);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (2, 'MENU_EMPLOYEE_INFORMATION_EDIT', 'Edit', 50);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (2, 'MENU_EMPLOYEE_INFORMATION_APPROVE', 'Approve', 60);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (2, 'MENU_EMPLOYEE_INFORMATION_RETURN', 'Return', 70);

insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (3, 'MENU_MY_DETAILS', 'My details', 1);

insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (4, 'MENU_EXPENSES', 'Expenses', 1);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (4, 'MENU_EXPENSES_CREATE_EXPENSES', 'Create Expenses', 80);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (4, 'MENU_EXPENSES_LIST_EXPENSES', 'List Expenses', 90);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (4, 'MENU_EXPENSES_EXPENSES_REPORT', 'Expenses Report', 100);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (4, 'MENU_EXPENSES_VIEW', 'View', 110);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (4, 'MENU_EXPENSES_UPDATE', 'Update', 120);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (4, 'MENU_EXPENSES_GENERATE_REPORT', 'Generate report', 130);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (4, 'MENU_EXPENSES_EXPORT_TO_CSV', 'Export to csv', 140);

insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (5, 'MENU_HOLIDAY', 'Holiday', 1);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (5, 'MENU_CREATE', 'Create', 150);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (5, 'MENU_PUBLISH', 'Publish', 160);

insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (6, 'MENU_PA', 'Performance Review', 1);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (6, 'MENU_PA_CREATE PERFORMANCE REVIEW', 'Create Performance Review', 170);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (6, 'MENU_PA_ADD_PERFORMANCE_REVIEW', 'Add Performance Review', 180);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (6, 'MENU_PA_DELETE_PERFORMANCE_REVIEW', 'Delete Performance Review', 190);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (6, 'MENU_PA_EDIT_PERFORMANCE_REVIEW', 'Edit Performance Review', 200);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (6, 'MENU_PA_ASSIGN_EMPLOYEE', 'Assign Employee', 210);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (6, 'MENU_PA_VIEW', 'View', 220);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (6, 'MENU_PA_PUBLIC', 'Public', 230);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (6, 'MENU_PA_REPORT', 'Report', 240);

insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (7, 'MENU_LEAVE', 'Leave', 1);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (7, 'MENU_LEAVE_VIEW_LEAVE', 'View Leave', 250);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (7, 'MENU_LEAVE_SEARCH', 'Search', 260);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (7, 'MENU_LEAVE_REPORT', 'Report', 270);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (7, 'MENU_LEAVE_REPORT_TO_CSV', 'Report to CSV', 280);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (7, 'MENU_LEAVE_ASSIGN_LEAVE', 'Assign Leave', 290);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (7, 'MENU_LEAVE_VIEW', 'View', 300);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (7, 'MENU_LEAVE_APPROVE', 'Approve', 310);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (7, 'MENU_LEAVE_REJECT', 'Reject', 320);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (7, 'MENU_LEAVE_LEAVE_BALANCE', 'Leave Balance', 330);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (7, 'MENU_LEAVE_LEAVE_LEDGER', 'Leave Ledger', 340);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (7, 'MENU_LEAVE_LEAVE_LEDGER_DELETE', 'Leave Ledger Delete', 350);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (7, 'MENU_LEAVE_MY_BALANCE', 'My Balance', 360);

insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (8, 'MENU_LEAVE_TYPE', 'Leave Type', 1);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (8, 'MENU_LEAVE_TYPE_ADD', 'Add', 370);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (8, 'MENU_LEAVE_TYPE_EDIT', 'Edit', 380);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (8, 'MENU_LEAVE_TYPE_VIEW', 'View', 390);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (8, 'MENU_LEAVE_TYPE_DELETE', 'Delete', 400);
  
  
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (9, 'MENU_COMP_OFF', 'Comp Off', 1);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (9, 'MENU_COMP_OFF_REQUEST', 'Request', 410);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (9, 'MENU_COMP_OFF_VIEW', 'View', 420);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (9, 'MENU_COMP_OFF_APPROVE', 'Approve', 430);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (9, 'MENU_COMP_OFF_REJECT', 'Reject', 440);
  
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (10, 'MENU_TIMESHEET', 'Timesheet', 1);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (10, 'MENU_TIMESHEET_EXPORT_TO_CSV_EMPLOYEE_REPORTS', 'Export To Csv Employee Reports ', 450);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (10, 'MENU_TIMESHEET_REPORTS', 'Reports', 460);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (10, 'MENU_TIMESHEET_VIEW', 'View', 470);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (10, 'MENU_TIMESHEET_APPROVE', 'Approve', 480);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (10, 'MENU_TIMESHEET_REJECT', 'Reject', 490);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (10, 'MENU_TIMESHEET_ACTIVITY_REPORT', 'Activity Report', 500);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (10, 'MENU_TIMESHEET_TIMESHEET_REPORT', 'Timesheet Report', 510);


insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (11, 'MENU_PROJECT_INFO', 'Project Info', 1);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (11, 'MENU_PROJECT_INFO_ADD', 'Add', 520);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (11, 'MENU_PROJECT_INFO_EDIT', 'Edit', 530);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (11, 'MENU_PROJECT_INFO_VIEW', 'View', 540);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (11, 'MENU_PROJECT_INFO_DELETE', 'Delete', 550);

insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (12, 'MENU_PROJECTS', 'Projects', 1);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (12, 'MENU_PROJECTS_ADD', 'Add', 560);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (12, 'MENU_PROJECTS_EDIT', 'Edit', 570);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (12, 'MENU_PROJECTS_VIEW', 'View', 580);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (12, 'MENU_PROJECTS_DELETE', 'Delete', 590);
  
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (13, 'MENU_CUSTOMERS', 'Customers', 1);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (13, 'MENU_CUSTOMERS_ADD', 'Add', 600);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (13, 'MENU_CUSTOMERS_EDIT', 'Edit', 610);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (13, 'MENU_CUSTOMERS_VIEW', 'View', 620);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (13, 'MENU_CUSTOMERS_DELETE', 'Delete', 630);

insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (14, 'MENU_ACTIVITY_LISTS', 'Activity Lists', 1);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (14, 'MENU_ACTIVITY_LISTS_ADD', 'Add', 640);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (14, 'MENU_ACTIVITY_LISTS_EDIT', 'Edit', 650);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (14, 'MENU_ACTIVITY_LISTS_VIEW', 'View', 660);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (14, 'MENU_ACTIVITY_LISTS_DELETE', 'Delete', 670);

insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (15, 'MENU_STATUS_UPDATE', 'Status Update', 1);

insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (16, 'MENU_MEALS_ORDER', 'Meals Order', 1);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (16, 'MENU_MEALS_ORDER_VIEW', 'View', 690);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (16, 'MENU_MEALS_ORDER_CONFIRM', 'Confirm', 700);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (16, 'MENU_MEALS_ORDER_CANCEL', 'Cancel', 710);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (16, 'MENU_MEALS_ORDER_REPORT_TOTAL_COUNT', 'Report Total Count', 720);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (16, 'MENU_MEALS_ORDER_REPORT_DETAILS', 'Report details', 730);
insert into  menu_list_item(menu_master_id, menu_key, name, sort_order) values (16, 'MENU_MEALS_ORDER_REPORT_REPORT_TO_CSV', 'Report to CSV', 740);




create table role_master(
  role_id bigint not null primary key  auto_increment,
  role_name varchar(125) default ''
);
