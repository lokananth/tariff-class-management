
CREATE TABLE IF NOT EXISTS `timesheet` (
  `timesheet_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` bigint NOT NULL,
  `employee_id` varchar(10) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,  
  `startTime` time NOT NULL,
  `endTime` time NOT NULL,
  `timeSpent` time NOT NULL,
  `description` text NOT NULL,
  `status` varchar(25) NOT NULL,
  `supervisor_comments` text NOT NULL,
  `submitted_date` date NOT NULL,
  `supervisor_updated_date` datetime NOT NULL,
  `project_name` varchar(25) NOT NULL,
  `activity_name` varchar(25) NOT NULL,
PRIMARY KEY (`timesheet_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT = 1 ;

ALTER TABLE login 
ADD `project_admin` varchar(25);


CREATE TABLE IF NOT EXISTS `project` (
  `project_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_name` varchar(55) NOT NULL,
  `customer_name` varchar(35) NOT NULL,
  `project_admin_name` varchar(50) NOT NULL,
  `description` text NOT NULL,
PRIMARY KEY (`project_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT = 1 ;

CREATE TABLE IF NOT EXISTS `customer` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(35) NOT NULL,
  `description` text NOT NULL,
PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT = 1 ;


CREATE TABLE IF NOT EXISTS `activity` (
  `activity_id` int(11) NOT NULL AUTO_INCREMENT,
  `activity_name` varchar(35) NOT NULL,
  `description` text NOT NULL,
PRIMARY KEY (`activity_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT = 1 ;