
delete from md_personal_details where convert(employee_id,unsigned) <= 100079;
delete from md_personal_details_history where convert(employee_id,unsigned) <= 100079;

delete from md_passport_details where convert(employee_id,unsigned) <= 100079;
delete from md_passport_details_history where convert(employee_id,unsigned) <= 100079;

delete from md_educational_details where convert(employee_id,unsigned) <= 100079;
delete from md_educational_details_history where convert(employee_id,unsigned) <= 100079;

delete from md_experience_details where convert(employee_id,unsigned) <= 100079;
delete from md_experience_details_history where convert(employee_id,unsigned) <= 100079;

delete from md_skillset_details where convert(employee_id,unsigned) <= 100079;
delete from md_skillset_details_history where convert(employee_id,unsigned) <= 100079;

delete from md_local_address_details where convert(employee_id,unsigned) <= 100079;
delete from md_local_address_details_history where convert(employee_id,unsigned) <= 100079;

delete from md_permanent_address_details where convert(employee_id,unsigned) <= 100079;
delete from md_permanent_address_details_history where convert(employee_id,unsigned) <= 100079;

delete from md_family_details where convert(employee_id,unsigned) <= 100079;
delete from md_family_details_history where convert(employee_id,unsigned) <= 100079;

delete from md_certificate_details where convert(employee_id,unsigned) <= 100079;
delete from md_certificate_details_history where convert(employee_id,unsigned) <= 100079;

delete from md_employee_submitted_data where convert(employee_id,unsigned) <= 100079;

INSERT INTO md_employee_submitted_data(id,employee_id,firstname,lastname,name,official_mail_id,role,status,admin_status) (select id,employee_id,firstname,lastname,username,email_id,role,'NotSubmitted','pending' from login where convert(employee_id,unsigned) <= 100079 and  
employee_id NOT IN(select employee_id from md_employee_submitted_data) and is_active = 1);