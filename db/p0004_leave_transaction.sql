create table leave_transaction (
  transaction_id bigint not null primary key  auto_increment,
  leave_id bigint not null default 0,
  employee_id bigint not null default 0,
  leave_type_id int not null default 0,
  leave_type_code varchar(10) default '',
  leave_date datetime ,
  status varchar(15) default '',
  admin_status varchar(15) default ''
);


