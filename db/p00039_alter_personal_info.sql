alter table md_personal_details change fullname username varchar(255) NOT NULL;
alter table md_personal_details add emergency_contact_name varchar(255) NOT NULL;
alter table md_personal_details add emergency_contact_no varchar(13) NOT NULL;