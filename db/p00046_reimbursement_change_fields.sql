alter table reimbursement_details change approve status varchar(30) not null;
alter table reimbursement_details change paid_amount requested_amount float(20,2) not null;