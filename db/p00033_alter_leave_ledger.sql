alter table leave_ledger add employee_id bigint NOT NULL;
alter table leave_ledger change ledger_type status varchar(25) NOT NULL;
alter table leave_ledger change leave_date date date NOT NULL;
alter table compoff_history add status varchar(20) NOT NULL;