create table status_update (
  status_update_id bigint not null AUTO_INCREMENT,
  employee_id bigint not null default 0,
  mail_to_id varchar(255) not null,
  mail_cc_id varchar(255) not null,
  status_update_date date not null,
  submitted_date datetime not null,
  status_message text not null,
  status varchar(15) not null,
  PRIMARY KEY (`status_update_id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

