create table leave_opening_balance (
  ob_id bigint not null primary key  auto_increment,
  employee_id bigint not null default 0,
  sl float(6,2) default 0,
  pl float(6,2) default 0,
  ml float(6,2) default 0
);


