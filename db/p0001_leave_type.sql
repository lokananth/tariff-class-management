create table leave_type (
  leave_type_id int not null primary key  auto_increment,
  leave_type_name varchar(60) not null default '',
  leave_type_code varchar(3) not null default ''
);

