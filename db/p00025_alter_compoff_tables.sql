
RENAME TABLE `compoff_details` TO `compoff`;
RENAME TABLE `compoff_form` TO `compoff_details`;

alter table compoff change total_date total_days int(8) NOT NULL;

ALTER TABLE compoff change id compoff_id int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE compoff drop cp_form_id;
ALTER TABLE compoff change approve status varchar(20);

ALTER TABLE compoff add requested_date datetime NOT NULL;


ALTER TABLE compoff change start_date start_date date NOT NULL;
ALTER TABLE compoff change end_date end_date date NOT NULL;
ALTER TABLE compoff change approved_date updated_date datetime NOT NULL;

alter table compoff drop approved_by;

ALTER TABLE compoff_details change id compoff_details_id bigint NOT NULL AUTO_INCREMENT;

ALTER TABLE compoff_details change type compoff_id int(11) NOT NULL default 0;
ALTER TABLE compoff_details change user_id compoff_leave_date date;



