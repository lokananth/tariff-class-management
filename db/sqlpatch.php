ALTER TABLE `performance_status` ADD `user_created_date` DATE NOT NULL , ADD `supervisor_created_date` DATE NOT NULL , ADD `admin_created_date` DATE NOT NULL
ALTER TABLE `performance_user_mapping` CHANGE `member_id` `employee_id` VARCHAR( 50 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL

ALTER TABLE `performance_supervisors` ADD `is_active` INT( 11 ) NOT NULL AFTER `created_by`

ALTER TABLE `performance_supervisors` CHANGE `is_active` `is_active` INT( 11 ) NOT NULL DEFAULT '0'

ALTER TABLE  `login` ADD  `is_active` INT NOT NULL DEFAULT  '1'

ALTER TABLE  `performance_supervisors` ADD  `report_too` VARCHAR( 20 ) NOT NULL AFTER  `supervisor_id`

---
ALTER TABLE `md_employee_submitted_data` ADD `admin_status` VARCHAR( 50 ) NOT NULL