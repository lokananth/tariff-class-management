ALTER TABLE timesheet 
change project_name project_id int(11) NOT NULL,
change activity_name activity_id int(11) NOT NULL;

ALTER TABLE login
change role_type role_type varchar(25);

ALTER TABLE project change customer_name customer_id int(12) NOT NULL;
ALTER TABLE project drop project_admin_name;

CREATE TABLE IF NOT EXISTS `timesheet_history`(
  `timesheet_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `timesheet_date` date NOT NULL,
  `user_id` bigint NOT NULL,
  `status` varchar(25) NULL,
  `supervisor_comments` text NULL,
  `supervisor_updated_date` datetime NULL,
PRIMARY KEY (`timesheet_history_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT = 1 ;


ALTER TABLE login
add report_to_id bigint NULL default 0,
add supervisor_mapid bigint NULL default 0,
change is_project_admin is_project_admin int(3) NULL default 0
