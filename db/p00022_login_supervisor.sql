ALTER table performance_supervisors change supervisor_id supervisor_mapids varchar(20) default 0;

ALTER table performance_user_mapping change supervisor_id supervisor_mapids varchar(20) default 0;

ALTER table login change supervisor_mapid supervisor_id bigint default 0;

ALTER table login drop report_to_id;