ALTER TABLE timesheet
change user_id user_id bigint,
drop firstname,
drop lastname,
drop username,
drop employee_id,
drop description,
drop activity_id,
drop project_id,
drop timeSpent;


ALTER TABLE timesheet change submitted_date timesheet_date date NOT NULL;
ALTER TABLE timesheet change supervisor_comments comments text NOT NULL;
ALTER TABLE timesheet change supervisor_updated_date updated_date datetime NOT NULL;

CREATE TABLE IF NOT EXISTS `timesheet_item`(
  `timesheet_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `timesheet_id` int(11) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `time_spent` time NOT NULL,
  `description` text NOT NULL,
  `project_id` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL,
  `task_reference` varchar(25) NOT NULL,
PRIMARY KEY (`timesheet_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT = 1 ;
 

  

ALTER TABLE timesheet_history change user_id timesheet_id bigint NOT NULL;
ALTER TABLE timesheet_history change supervisor_comments comments text;
ALTER TABLE timesheet_history change supervisor_updated_date timesheet_updated_date datetime NOT NULL;
ALTER TABLE timesheet_history add updated_by_id bigint NOT NULL;
ALTER TABLE timesheet_history drop timesheet_date;

ALTER TABLE timesheet drop startTime;
ALTER TABLE timesheet drop endTime;
