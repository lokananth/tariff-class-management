CREATE TABLE meals_order (
  meals_order_id bigint NOT NULL AUTO_INCREMENT,
  employee_id bigint NOT NULL default 0,
  start_date datetime,
  end_date datetime,
  meals_type varchar(10) default '',
  status varchar(10) default '', 
PRIMARY KEY (meals_order_id)
) ENGINE=InnoDB;


CREATE TABLE meals_order_details (
  meals_order_details_id bigint NOT NULL AUTO_INCREMENT,
  meals_order_id bigint NOT NULL default 0,
  employee_id bigint NOT NULL default 0,
  date datetime,
  meals_type varchar(10) default '',
  status varchar(10) default '', 
PRIMARY KEY (meals_order_details_id)
) ENGINE=InnoDB;