create table employee_leave (
  leave_id bigint not null primary key  auto_increment,
  employee_id bigint not null,
  start_date datetime not null,
  end_date datetime not null,
  leave_type_id int not null,
  notes text default '',
  status varchar(10) default '',
  days int default 0
);


