alter table md_personal_details add first_name varchar(60) default '';
alter table md_personal_details add last_name varchar(60) default '';
alter table md_personal_details add date_of_joining datetime;

alter table login add date_of_joining datetime;

