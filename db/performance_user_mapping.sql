-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 07, 2013 at 10:41 AM
-- Server version: 5.5.28
-- PHP Version: 5.4.6-1ubuntu1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `BambeeQ`
--

-- --------------------------------------------------------

--
-- Table structure for table `performance_user_mapping`
--

CREATE TABLE IF NOT EXISTS `performance_user_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supervisor_id` varchar(50) NOT NULL,
  `member_id` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='User Mapping Table' AUTO_INCREMENT=4 ;

--
-- Dumping data for table `performance_user_mapping`
--

INSERT INTO `performance_user_mapping` (`id`, `supervisor_id`, `member_id`) VALUES
(1, '100010', '100043'),
(2, '100010', '100047');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
