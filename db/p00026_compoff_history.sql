drop table IF EXISTS compoff_details;

CREATE TABLE IF NOT EXISTS `compoff_history`(
  `compoff_history_id` bigint NOT NULL AUTO_INCREMENT,
  `compoff_id` int(11) NOT NULL,
  `comments` text NOT NULL,
  `updated_date` datetime NOT NULL,
  `updated_by_id` int(11) NOT NULL,
  
PRIMARY KEY (`compoff_history_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT = 1 ;