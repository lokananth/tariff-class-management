CREATE TABLE IF NOT EXISTS `project_admin` (
  project_admin_id int(11) NOT NULL AUTO_INCREMENT,
  project_id int(11) NOT NULL,
  admin_id varchar(10) NOT NULL,
  PRIMARY KEY (project_admin_id)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

