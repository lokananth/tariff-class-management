
drop table IF EXISTS employee_leave;
drop table IF EXISTS leave_transaction;
drop table IF EXISTS leave_type;
drop table IF EXISTS leave_opening_balance;

CREATE TABLE IF NOT EXISTS `leave_transaction`(
  `leave_transaction_id` bigint NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `leave_type_id` int(8) NOT NULL,
  `requested_date` datetime NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `total_days` float(6,2) NOT NULL,
  `status` varchar(20) NOT NULL,
  `comments` text NOT NULL,
  `admin_comments` text NOT NULL,
  `updated_date` datetime NOT NULL,
  
PRIMARY KEY (`leave_transaction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT = 1 ;

CREATE TABLE IF NOT EXISTS `leave_type`(
  `leave_type_id` bigint NOT NULL AUTO_INCREMENT,
  `leave_type_name` varchar(20) NOT NULL,
  `leave_type_code` varchar(10) NOT NULL,
  `is_carry_forward` int(2) NOT NULL,
PRIMARY KEY (`leave_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT = 1 ;

CREATE TABLE IF NOT EXISTS `leave_ledger`(
  `leave_ledger_id` bigint NOT NULL AUTO_INCREMENT,
  `leave_transaction_id` int(11) NOT NULL,
  `leave_type_id` int(11) NOT NULL,
  `leave_date` date NOT NULL,
  `days` float(6,2) NOT NULL,
  `ledger_type` varchar(15) NOT NULL,
PRIMARY KEY (`leave_ledger_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT = 1 ;


CREATE TABLE IF NOT EXISTS `leave_balance`(
  `leave_balance_id` bigint NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `leave_type_id` int(8) NOT NULL,
  `year` date NOT NULL,
  `balance` float(6,2) NOT NULL,
  `carry_forward` float(6,2) NOT NULL,
PRIMARY KEY (`leave_balance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT = 1 ;

CREATE TABLE IF NOT EXISTS `leave_history`(
  `leave_history_id` bigint NOT NULL AUTO_INCREMENT,
  `leave_transaction_id` int(11) NOT NULL,
  `updated_by_id` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  `status` varchar(20) NOT NULL,
  `comments` text NOT NULL,
PRIMARY KEY (`leave_history_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT = 1 ;
